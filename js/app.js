if ("serviceWorker" in navigator) {
  window.addEventListener("load", function () {
    navigator.serviceWorker
      .register("/serviceWorker.js")
      .then(res => console.log("service worker registered"))
      .catch(err => console.log("service worker not registered", err));
  });
}

function init() {
  const semaforo = document.getElementById("semaforo");
  change(semaforo);
}

function testnetwork(link) {
  if (!window.navigator.onLine) {
    alertify.alert('Errore!', 'Spiacenti, devi collegarti ad internet per visualizzare questo contenuto');
  } else {
    if (window.matchMedia('(display-mode: standalone)').matches) {
      location.replace(link)
    }
    else {
      window.open(link, "_blank");
    }
  }
}

function change(semaforo) {
  var number = Math.floor(Math.random() * 45) + 15;
  console.log("Cambio tra " + number + " secondi");
  number = number * 1000;
  setTimeout(function () {
    if (document.getElementById("auto").checked)
      if (semaforo.getAttribute("src").search("red") > 0) {
        semaforo.setAttribute("src", "images/green.svg");
        change(semaforo);
      }
      else if (semaforo.getAttribute("src").search("green") > 0) {
        semaforo.setAttribute("src", "images/yellow.svg");
        setTimeout(function () {
          semaforo.setAttribute("src", "images/red.svg");
          change(semaforo);
        }, 5000);
      } else if (semaforo.getAttribute("src").search("yellow") > 0) {
        semaforo.setAttribute("src", "images/red.svg");
        change(semaforo);
      }
  }, number);
}

function changeclick() {
  if (semaforo.getAttribute("src").search("red") > 0)
    semaforo.setAttribute("src", "images/green.svg");
  else if (semaforo.getAttribute("src").search("green") > 0)
    semaforo.setAttribute("src", "images/yellow.svg");
  else if (semaforo.getAttribute("src").search("yellow") > 0)
    semaforo.setAttribute("src", "images/red.svg");
}