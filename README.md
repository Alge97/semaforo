# Semaforo
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
[![Netlify Status](https://api.netlify.com/api/v1/badges/3dfafbd1-5aab-4fb6-941c-aad3aacbe4c2/deploy-status)](https://app.netlify.com/sites/semaforo/deploys)  
Il vostro semaforo personale disponibile [qui](https://semaforo.netlify.com/).

## Rigraziamenti
### Codice
Il progetto si appoggia al codice di ibrahima92 diponibile [qui](https://github.com/ibrahima92/pwa-with-vanilla-js)

### Media
Le immagini sono state recuperate da pngtree.com e flyclipart.com
